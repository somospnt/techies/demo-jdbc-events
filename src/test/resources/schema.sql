drop table persona if exists;

create table persona(
    id bigint identity primary key,
    nombre varchar(100),
    apellido varchar(100)
);

insert into persona (id, nombre, apellido)
values (1, 'Tony', 'awk');