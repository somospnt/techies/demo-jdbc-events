package com.somospnt.jdbc.event.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.somospnt.jdbc.event.domain.Persona;
import static org.assertj.core.api.Assertions.assertThat;

import com.somospnt.jdbc.event.repository.PersonaRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;

@SpringBootTest
@ExtendWith(OutputCaptureExtension.class)
public class PersonaServiceTest {

    @Autowired
    private PersonaService personaService;

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void guardar_guardaYEscribeEnElLog(CapturedOutput output) throws JsonProcessingException {
        Persona persona = new Persona();
        persona.setNombre("Steve");
        persona.setApellido("Caballer");
        personaService.guardar(persona);
        assertThat(personaRepository.findById(persona.getId()).get().getId()).isEqualTo(2L);
        assertThat(output).contains(this.objectMapper.writeValueAsString(persona));
    }

    @Test
    public void actualizar_ActualizaYEscribeEnElLog(CapturedOutput output) throws JsonProcessingException {
        Persona persona = new Persona();
        persona.setId(1L);
        persona.setNombre("Tony");
        persona.setApellido("Hawk");
        personaService.actualizar(persona);
        assertThat(personaRepository.findById(persona.getId()).get().getApellido()).isEqualTo("Hawk");
        assertThat(output).contains(this.objectMapper.writeValueAsString(persona));
    }

}
