package com.somospnt.jdbc.event.repository;

import com.somospnt.jdbc.event.domain.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long>{
    
}
