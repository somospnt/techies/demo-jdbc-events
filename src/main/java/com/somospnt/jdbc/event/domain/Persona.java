package com.somospnt.jdbc.event.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Persona {
    
    @Id
    private Long id;
    private String nombre;
    private String apellido;
    
    
}
