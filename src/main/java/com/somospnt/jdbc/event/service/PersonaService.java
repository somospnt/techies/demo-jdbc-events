package com.somospnt.jdbc.event.service;

import com.somospnt.jdbc.event.domain.Persona;
import com.somospnt.jdbc.event.repository.PersonaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PersonaService {
    
    private final PersonaRepository personaRepository;
    
    public void guardar(Persona persona) {
        personaRepository.save(persona);
    }

    public void actualizar(Persona persona) {
        personaRepository.save(persona);
    }
}
