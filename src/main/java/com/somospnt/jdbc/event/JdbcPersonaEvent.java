package com.somospnt.jdbc.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.somospnt.jdbc.event.domain.Persona;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.relational.core.mapping.event.AfterSaveCallback;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class JdbcPersonaEvent implements AfterSaveCallback<Persona> {

    private final ObjectMapper objectMapper;

    @Override
    public Persona onAfterSave(Persona persona) {
        log.info(toJsonString(persona));
        return persona;
    }

    private String toJsonString(Persona persona) {
        try {
            return this.objectMapper.writeValueAsString(persona);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(JdbcPersonaEvent.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

}
